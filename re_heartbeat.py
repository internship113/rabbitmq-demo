

#!/usr/bin/env python
import pika
import time

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost',heartbeat=3))
channel = connection.channel()

channel.exchange_declare(exchange='logs', exchange_type='fanout')


result = channel.queue_declare(queue='Test heartbeat', exclusive=False)
queue_name = result.method.queue

channel.queue_bind(exchange='logs', queue=queue_name)

print("Queue name: "+queue_name)

print(' [*] Waiting for logs. To exit press CTRL+C')

def callback(ch, method, properties, body):
    
    print(" [x] %r" % body)
    time.sleep(2)
    channel.basic_qos(prefetch_count=1)
    time.sleep(2)
    channel.basic_qos(prefetch_count=1)
    time.sleep(2)
    channel.basic_qos(prefetch_count=1)
    time.sleep(2)
    channel.basic_qos(prefetch_count=1)
    time.sleep(2)
    channel.basic_ack(multiple=False,delivery_tag=method.delivery_tag)

channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=False)

channel.start_consuming()
