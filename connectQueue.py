import pika

ip = '127.0.0.1'
port = '5672'
vhost = '/'
username = 'guest'
password = 'guest'
queue_name = 'TEST1'

url = 'amqp://' + username + ':' + password + '@' + ip + ':5672/'
params = pika.URLParameters(url=url)

params = pika.connection.ConnectionParameters(
    host=ip,port=int(port),
    credentials=pika.credentials.PlainCredentials(username, password),
)
QueueName = "TEST1"
connection = None
channel = None


connection = pika.BlockingConnection(params)
channel = connection.channel()
# channel.queue_declare(queue=queue_name, durable=False)

def callback(ch, method, properties, body):
    data = str(body.decode());
    print(data)
    channel.basic_ack(multiple=True)

channel.basic_consume(queue_name, callback, auto_ack=True)
channel.start_consuming()
connection.close()