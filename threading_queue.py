import time
import pika
import threading

heartbeat_time = 3
sleep_time = 10

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost',heartbeat=4))
channel = connection.channel()

channel.exchange_declare(exchange='logs', exchange_type='fanout')


result = channel.queue_declare(queue='Test heartbeat', exclusive=False)
queue_name = result.method.queue

channel.queue_bind(exchange='logs', queue=queue_name)

def pre_fetch():
    while True:
        if stop_thread :
            break
        print("Pre fetch!")
        channel.basic_qos(prefetch_count=1)
        time.sleep(2)

def body_message(body,method):
    time.sleep(sleep_time)
    print(" [x] %r" % body)
    channel.basic_ack(multiple=False,delivery_tag=method.delivery_tag)

def callback(ch, method, properties, body):
    body_str = body.decode('utf-8')
    global stop_thread
    stop_thread = False
    t1 = threading.Thread(target=pre_fetch, args=())
    t2 = threading.Thread(target=body_message, args=(body_str,method,))

    t1.start()
    t2.start()
    t2.join()
    stop_thread = True
    t1.join()
    print("Thread killed!")


channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=False)

channel.start_consuming()