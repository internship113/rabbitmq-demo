#!/usr/bin/env python
import pika
import time
import json

for i in range(0,1):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    # channel.queue_declare(queue='ha_stream.get_speech_recognition',durable=True)
    # channel.queue_declare(queue='ha_stream.get_speech_recognition.process',durable=True)
    # channel.queue_declare(queue='ha_stream.get_speech_recognition.store',durable=True)
    # channel.queue_declare(queue='ytstream.stream.added.data-partitioner',durable=True)

    # message = {
    #     "link": "https://www.youtube.com/watch?v=EMXDtuBHHgI",
    #     "text": "สวัสดี คาบ เพชร ดี คอมพิวเตอร์ จังหวัด สลิน นะ ครับ ผม",
    #     "_id" : "UCtpN5Fe2rr0DShETh4n-eIQ_EMXDtuBHHgI_1",
    #     "status": "pass",
    #     "error_message": ""
    # }
    message = {
            "link": "https://www.youtube.com/watch?v=EMXDtuBHHgI",
            "_id" : "UCtpN5Fe2rr0DShETh4n-eIQ_EMXDtuBHHgI_1"
        }
        # channel.basic_publish("")
    #short, medium, long, BFG long
    # path =['example_youtube1.json','example_youtube2.json','example_youtube3.json','example_youtube4.json']
    # path =['example_youtube1.json']
    # for i in path:
    #     f = open(i)
    #     message = json.load(f)
    #     channel.basic_publish(exchange='ha_ytstream', routing_key='ytstream.stream.added.data-partitioner', body=json.dumps(message).encode())
    #     print(" [x] Sent "+ str(message))
    channel.basic_publish(exchange='', routing_key='ha_stream.get_speech_recognition.process', body=json.dumps(message).encode())
    print(" [x] Sent "+ str(message))
    connection.close()
    # time.sleep(0.1)