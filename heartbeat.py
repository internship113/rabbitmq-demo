#!/usr/bin/env python
import pika
import time

for i in range(0,1):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='Test ysr')

    channel.basic_publish(exchange='', routing_key='Test ysr', body='Hello World! '+str(i))
    print(" [x] Sent 'Hello World!'"+str(i))
    connection.close()
    # time.sleep(0.1)